package com.alexscottrhodes.copyright_plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * A Maven Goal that adds or removes a custom copyright header to project files when the project is built. The copyright body is defined in a configuration file
 * that must be present somewhere within the project's root directory called "copyright-config.xml". For more information and example usage, see:
 * www.bitbucket.com/
 * @Goal cr
 * 
 * @phase test
 */
@Mojo(name="cr")
public class CopyrightGenerator extends AbstractMojo {

	@Parameter(defaultValue="${remove}")
	private boolean remove = false;

	@Parameter(defaultValue="${basedir}",required=true)
	private File sourceDir = new File("");

	private ArrayList<String> fileNames = new ArrayList<String>();
	private HashMap<String,String> copyrightText = new HashMap<String,String>();
	public void execute() throws MojoExecutionException {
		getLog().debug("remove = " + remove);
		if (!sourceDir.exists()) {
			getLog().error("Could not find source directory specified in POM");
		}
		populateFileList(sourceDir);
		Iterator<String> it = fileNames.iterator();
		while (it.hasNext()) {
			String name = it.next();
			if(name.contains("copyright-config.xml")){
				populateCopyrights(name);
				if(copyrightText.size()<1){
					getLog().error("Copyright confiruation invalid.");
					return;
				}
			}
			if (name.lastIndexOf(".") == -1) {
				it.remove();
			}
			String ext = name.substring(name.lastIndexOf("."));
			if (!(ext.contains("xhtml") || ext.contains("html") || ext.contains("xml") || ext.contains("java"))) {
				it.remove();
			}
		}
		for (String fname : fileNames) {
			if(remove){
				stripHeader(fname);
			}else{
				writeHeader(fname);
			}
		}
	}

	public void populateFileList(File root) {
		if (root.isFile()) {
			fileNames.add(root.getPath());
			return;
		}
		for (final File file : root.listFiles()) {
			if (file.isDirectory()) {
				populateFileList(file);
			} else {
				fileNames.add(file.getPath());
			}
		}
	}

	public void writeHeader(String fileName) {
			String header = getBody(fileName);
			if(header == null){
				return;
			}
			try {
				if(fileName.contains("copyright-plugin-backup") || fileName.contains("copyright-config.xml")){
					return;
				}
				File edit = new File(fileName);
				File backupDest = new File(sourceDir.getAbsolutePath()+"/copyright-plugin-backup/");
				backupDest.mkdirs();
				FileUtils.copyFileToDirectory(edit, backupDest);
				FileInputStream fis = new FileInputStream(edit);
				BufferedReader br = new BufferedReader(new InputStreamReader(fis));
				String line;
				String testSequence = "";
				while((line = br.readLine())!=null){
					testSequence+= line;
				}
				testSequence = sanitize(testSequence);
				String testHeader = sanitize(new String(header));			
				if(testSequence.contains(testHeader)){
					br.close();
					fis.close();
					return;
				}
				br.close();
				fis.close();
				fis = new FileInputStream(edit);
				br = new BufferedReader(new InputStreamReader(fis));
				PrintWriter writer = new PrintWriter("tmp");

				writer.println(header);
				while ((line = br.readLine()) != null) {
					writer.println(line);
				}
				writer.close();
				br.close();
				fis.close();
				edit.delete();
				File temp = new File("tmp");
				Path source = Paths.get(temp.getAbsolutePath());
				Files.move(source, source.resolveSibling(fileName));
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	
	public void stripHeader(String fileName) {
		String header = getBody(fileName);
		if(header == null){
			return;
		}
		try {
			if(fileName.contains("copyright-plugin-backup") || fileName.contains("copyright-config.xml")){
				return;
			}
			File edit = new File(fileName);
			File backupDest = new File(sourceDir.getAbsolutePath()+"/copyright-plugin-backup/");
			backupDest.mkdirs();
			FileUtils.copyFileToDirectory(edit, backupDest);
			FileInputStream fis = new FileInputStream(edit);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			PrintWriter writer = new PrintWriter("tmp");
			String line;
			while ((line = br.readLine()) != null) {
				if(!header.contains(line)){
					writer.println(line);
				}
			}
			writer.close();
			br.close();
			fis.close();
			edit.delete();
			File temp = new File("tmp");
			Path source = Paths.get(temp.getAbsolutePath());
			Files.move(source, source.resolveSibling(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
}
	
	
	public String getBody(String fileName){
		if (fileName.lastIndexOf(".") == -1) {
			return null;
		}
		String ext = fileName.substring(fileName.lastIndexOf("."));
		ext = ext.replace(".", "");
		String body = copyrightText.get(ext);
		if(body == null){
			return body;
		}
		if(ext.equalsIgnoreCase("java")){
			body = "/*" + body + "*/";
		}else if(ext.equalsIgnoreCase("xml") || ext.equalsIgnoreCase("xhtml") || ext.equalsIgnoreCase("html")){
			body = "<!-- \n" + body + "\n -->";
		}
		return body;
	}
	
	public void populateCopyrights(String configFileName){
		try{
			  SAXBuilder builder = new SAXBuilder();
			  File config = new File(configFileName);
			  Document document = (Document) builder.build(config);
			  Element root = document.getRootElement();
			  @SuppressWarnings("rawtypes")
			  List copyrightElements = root.getChildren("copyright");
			  for(int i = 0; i < copyrightElements.size(); i++){
				 
				  Element cp = (Element) copyrightElements.get(i);
				  String ext = cp.getChildText("file-type");
				  if(ext == null){
					  throw new Exception("Invalid copyright configuration file.");
				  }
				  if(!(ext.equals("xhtml") || ext.equals("html") || ext.equals("xml") || ext.equals("java"))){
					  throw new Exception("Unsupproted file type in copyright configuration file.");
				  }
				  String body = cp.getChildText("body");
				  if(body == null || body==""){
					  continue;
				  }else{
					  copyrightText.put(ext, body);
				  }
			  }
			
		}catch(Exception e){
			getLog().error(e.getMessage());
		}
	}
	
	public String sanitize(String in){
		in = in.replace("\n", "").replace("\r", "").replace(" ","").replaceAll("	","");
		return in;
	}
}
